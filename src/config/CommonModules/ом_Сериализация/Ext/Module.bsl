﻿// Сериализация/десериализация значений, XDTO, XSD
//  
// BSLLS:NestedFunctionInParameters-off - без лишних переменных выполнение быстрее
// BSLLS:EmptyRegion-off - пустые области разрешены
// BSLLS:CommonModuleNameClientServer-off - без необходимости постфиксы не добавляем

#Область ПрограммныйИнтерфейс

// Возвращает строку сериализации значения на основании глобальной фабрики
// 
// Параметры:  
// 	Значение - Произвольный - сериализуемое значение
// 	Фабрика - ФабрикаXDTO - фабрика преобразования, при отсутствии используется глобальная фабрика
// 
// Возвращаемое значение:
//   Строка
// 
Функция ЗначениеВXML(Значение, Фабрика = Неопределено) Экспорт
	
	#Если ВебКлиент Тогда
	 	Результат	= ом_СериализацияВызовСервера.ЗначениеВXML(Значение, Фабрика);
	#Иначе
		
		Запись			= Новый ЗаписьXML;
		Запись.УстановитьСтроку("UTF-8");
		Сериализатор(Фабрика).ЗаписатьXML(Запись, Значение, НазначениеТипаXML.Явное);
		
		Результат	= Запись.Закрыть();
	#КонецЕсли 
	
	Возврат Результат;
	
КонецФункции // ЗначениеВXML 

// Десериализует значение из строки
// 
// Параметры:  
// 	Строка - Строка - сериализованное представление значения
// 	Фабрика - ФабрикаXDTO - фабрика преобразования, при отсутствии используется глобальная фабрика
// 
// Возвращаемое значение:
//   Произвольный
// 
Функция XMLВЗначение(Строка, Фабрика = Неопределено) Экспорт
	
	#Если ВебКлиент Тогда
		Результат	= ом_СериализацияВызовСервера.XMLВЗначение(Строка, Фабрика);
	#Иначе
		ЧтениеXML	= Новый ЧтениеXML;
		ЧтениеXML.УстановитьСтроку(Строка);
		Результат	=  Сериализатор(Фабрика).ПрочитатьXML(ЧтениеXML);
	#КонецЕсли 
	
	Возврат Результат;
	
КонецФункции // XMLВЗначение 

// Возвращает схему XML по тексту XSD схемы
// 
// Параметры: 
// 	Описание - Строка - XSD схема
// 
// Возвращаемое значение: 
// 	СхемаXML
// 
Функция ОпределениеСхема(Текст) Экспорт 
	
	Чтение	= Новый ЧтениеXML;
	Чтение.УстановитьСтроку(Текст);
	
	Построитель			= Новый ПостроительDOM;
	Документ			= Построитель.Прочитать(Чтение);
	СхемыПостроитель	= Новый ПостроительСхемXML;
	
	Возврат СхемыПостроитель.СоздатьСхемуXML(Документ);
	
КонецФункции // ОпределениеСхема 

// Возвращает фабрику XDTO по тексту XSD схемы
// 
// Параметры: 
// 	Описание - Строка - текст схемы XSD
// 
// Возвращаемое значение: 
// 	ФабрикаXDTO
// 
Функция ОпределениеФабрика(Текст) Экспорт 
	
	Набор	= Новый НаборСхемXML;
	Набор.Добавить(ОпределениеСхема(Текст));
	
	Возврат Новый ФабрикаXDTO(Набор);
	
КонецФункции // ОпределениеФабрика 

// Возвращает объект сериализатора XDTO на основании фабрики
// 
// Параметры: 
// 	Фабрика - ФабрикаXDTO - фабрика преобразования, при отсутствии используется глобальная
// 
// Возвращаемое значение: 
// 	СериализаторXDTO
// 
Функция Сериализатор(Фабрика = Неопределено) Экспорт
	
	#Если ВебКлиент Тогда
		ВызватьИсключение ом_СериализацияТексты.СредаСериализаторНедоступен();
	#КонецЕсли
	
	Возврат ?(Фабрика = Неопределено
	, ом_СериализацияПовтИсп.Сериализатор()
	, Новый СериализаторXDTO(Фабрика));
	
КонецФункции // Сериализатор 

#КонецОбласти

#Область СлужебныйПрограммныйИнтерфейс

#КонецОбласти

#Область СлужебныеПроцедурыИФункции

#КонецОбласти

// BSLLS:CommonModuleNameClientServer-on
// BSLLS:EmptyRegion-on
// BSLLS:NestedFunctionInParameters-on

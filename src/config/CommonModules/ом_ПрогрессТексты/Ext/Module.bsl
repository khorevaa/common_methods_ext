﻿// Отображаемые тексты прогресса выполнения
//  
// BSLLS:NestedFunctionInParameters-off - без лишних переменных выполнение быстрее
// BSLLS:EmptyRegion-off - пустые области разрешены
// BSLLS:CommonModuleNameClientServer-off - без необходимости постфиксы не добавляем

#Область ПрограммныйИнтерфейс

#КонецОбласти

#Область СлужебныйПрограммныйИнтерфейс

// Возвращает текст оишбки неверного вызова формы
//
// Параметры: 
// 	ЯзыкКод - Строка - Код языка
//
// Возвращаемое значение: 
// 	Строка
//
Функция ФормаОткрытиеПараметрыОшибка(ЯзыкКод = "") Экспорт
	
	Возврат НСтр("ru = 'Неверный вызов формы!'", ЯзыкКод);
	
КонецФункции // ФормаОткрытиеПараметрыОшибка 

// Возвращает информацию об ожидании статус длительной операции
//
// Параметры: 
// 	ЯзыкКод - Строка - Код языка
//
// Возвращаемое значение: 
// 	Строка
//
Функция ИнформацияОперацияСтатусОжидание(ЯзыкКод = "") Экспорт
	
	Возврат НСтр("ru = 'Пожалуйста подождите. Выполняется получение статуса длительной операции...'", ЯзыкКод);
	
КонецФункции // ИнформацияОперацияСтатусОжидание 

// Возвращает информацию об отсутствии длительной операции
//
// Параметры: 
// 	ЯзыкКод - Строка - Код языка
//
// Возвращаемое значение: 
// 	Строка
//
Функция ИнформацияОперацияОтсутствует(ЯзыкКод = "") Экспорт
	
	Возврат НСтр("ru = 'Ошибка открытия прогресса выполнения: "
	+ "не удалось получить информацию о прогрессе выполнения длительной операции. "
	+ "Обратитесь к разработчикам.'", ЯзыкКод);
	
КонецФункции // ИнформацияОперацияОтсутствует 

// Возвращает ошибку среды выполнения при создании оповещения
//
// Параметры: 
// 	ЯзыкКод - Строка - Код языка
//
// Возвращаемое значение: 
// 	Строка
//
Функция ОповещениеСредаОшибка(ЯзыкКод = "") Экспорт
	
	Возврат НСтр("ru = 'Создание оповещений прогресса длительных операций доступно для "
	+ "вызова только из контекста длительной операции! Обратитесь к разработчикам.'", ЯзыкКод);
	
КонецФункции // ОповещениеСредаОшибка 

#КонецОбласти

#Область СлужебныеПроцедурыИФункции

#КонецОбласти

// BSLLS:CommonModuleNameClientServer-on
// BSLLS:EmptyRegion-on
// BSLLS:NestedFunctionInParameters-on

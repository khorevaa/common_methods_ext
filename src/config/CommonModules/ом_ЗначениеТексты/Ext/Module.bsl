﻿// Отображаемые тексты модуля ом_Значение 
//  
// BSLLS:NestedFunctionInParameters-off - без лишних переменных выполнение быстрее
// BSLLS:EmptyRegion-off - пустые области разрешены
// BSLLS:CommonModuleNameClientServer-off - клиент-серверные модули именум без постфиксов

#Область ПрограммныйИнтерфейс

#КонецОбласти

#Область СлужебныйПрограммныйИнтерфейс

// Возвращает текст недопустимой среды выполнения при проверке значения на принадлежность объктам
// 
// Параметры: 
// 	ЯзыкКод - Строка - код языка
// 
// Возвращаемое значение: 
// 	Строка
// 
Функция ОбъектЭтоСредаНедопустима(ЯзыкКод = "") Экспорт
	
	Возврат НСтр("ru = 'Невозможно проверить является ли значение объектом в среде выполнения " 
	+ ом_СредаВыполнения.Представление( , ЯзыкКод) + "! Обратитесь к разработчикам.'", ЯзыкКод);
	
КонецФункции // ОбъектЭтоСредаНедопустима 

#КонецОбласти

#Область СлужебныеПроцедурыИФункции

#КонецОбласти

// BSLLS:CommonModuleNameClientServer-on
// BSLLS:EmptyRegion-on
// BSLLS:NestedFunctionInParameters-on

﻿// Служебные методы подсистемы. Серверная часть
//  
// BSLLS:NestedFunctionInParameters-off - без лишних переменных выполнение быстрее
// BSLLS:EmptyRegion-off - пустые области разрешены

#Область ПрограммныйИнтерфейс

#КонецОбласти

#Область СлужебныйПрограммныйИнтерфейс

// Инициализация подсистемы
// 
// Параметры: 
// 
Процедура Инициализировать() Экспорт
	
	ом_ТипАдаптерВызовСервера.Инициализировать();
	ом_МетаданныеАдаптер.Инициализировать();
	ом_ПрогрессДиалогВызовСервера.Инициализировать();
	
КонецПроцедуры // Инициализировать 

// Инициализация вида и адаптера
// 
// Параметры: 
// 	Вид - Строка - Идентификатор вида адаптеров
// 	Наименование - Строка - Наименование вида адаптеров
//  ОбработкаИмя - Строка - Имя встроенной обработки адаптера
// 	ФормаИмя - Строка - Имя формы 
// 
Процедура АдаптерИнициализировать(Вид, Наименование, ОбработкаИмя, ФормаИмя = Неопределено) Экспорт
	
	УстановитьПривилегированныйРежим(Истина);
	Если НЕ ом_АдаптерВид.Зарегистрирован(Вид) Тогда
		ом_АдаптерВид.Добавить(Вид, Наименование);
	КонецЕсли;
	Если НЕ ом_АдаптерАктивный.Назначен(Вид) Тогда
		Идентификатор	= ом_АдаптерОбъект.Идентификатор(Вид, ОбработкаИмя, Истина, ФормаИмя);
		Если НЕ ом_АдаптерОбъект.Зарегистрирован(Идентификатор) Тогда
			Идентификатор	= ом_АдаптерОбъект.Добавить(Вид, ОбработкаИмя, Истина, ФормаИмя);
		КонецЕсли;
		ом_АдаптерАктивныйСервер.Установить(Вид, Идентификатор);
	КонецЕсли;
	
КонецПроцедуры // АдаптерИнициализировать 

#КонецОбласти

#Область СлужебныеПроцедурыИФункции

#КонецОбласти
// BSLLS:EmptyRegion-on
// BSLLS:NestedFunctionInParameters-on
